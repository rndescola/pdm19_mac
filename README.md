# Programação dispositivos moveis II 1819 #

## Links uteis ##

[Criar documentação em swift ](https://nshipster.com/swift-documentation/)


## informações Importantes ##

Podem usar o [Slack](https://tinyurl.com/slackpdm19) para tirar duvidas

[form](http://tinyurl.com/y2tkxg86) para submeter os repositorios

## Slides ##


[Swift 101](https://bitbucket.org/GoncaloF/pdm19/downloads/Swift%20101.pdf)

[Class](https://bitbucket.org/GoncaloF/pdm19/downloads/Aula%204%20-%20swift_class.pdf)


### Exemplos  ###

[app1 - Ola mundo ](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app1/)

[app2 - layout por codigo](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app2/)

[app3 - dismiss keyboard](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app3/)

[app4 - tabelas](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app4/)

[app5 - Navegação](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app5/)

[app6 - Gestão Erro](https://bitbucket.org/GoncaloF/pdm19/src/master/exemplos/app6/)



## Fichas de trabalho ##

[Ficha 1](https://bitbucket.org/GoncaloF/pdm19/downloads/ISTEC_ficha_funcs.pdf)

[Ficha 2](https://bitbucket.org/GoncaloF/pdm19/downloads/ISTEC_ficha_collections.pdf)


## Trabalhos ##

[Trabalho 2](https://bitbucket.org/GoncaloF/pdm19/downloads/ISTEC_tp2_ddm1819.pdf)

[Trabalho 3 - V3 ](https://bitbucket.org/GoncaloF/pdm19/downloads/ISTEC_tp3_ddm1819.pdf)


### Solução Fichas de trabalho ###





