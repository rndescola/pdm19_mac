//
//  ViewController.swift
//  app1_LabB
//
//  Created by Gonçalo Feliciano on 07/05/2019.
//  Copyright © 2019 Gonçalo Feliciano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var txtf: UITextField!
    @IBOutlet weak var btno: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.lbl.text = "ola"
        self.lbl.backgroundColor = UIColor.black
        self.lbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        self.lbl.numberOfLines = 3
        self.txtf.placeholder = "Digite o seu nome"
        self.txtf.frame.size = CGSize(width: self.txtf.frame.width, height: (self.txtf.frame.height * 3))
    }
    
    @IBAction func btn(_ sender: UIButton) {
  
        self.lbl.text = self.txtf.text
    }

}

