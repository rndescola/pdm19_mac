import UIKit

class ViewController: UIViewController {
    
 //   @IBOutlet weak var lbl: UILabel!
    var lbl:UILabel?
    var btn:UIButton?
    lazy var arr:[UIView] = [lbl!,btn!]
    var  c = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pad = 50
        let w = Int(self.view.frame.width) - (2 * pad)
        let y = Int.random(in: 10...200)
        let btny = y + 200
        
        //MARK: lbls
        self.lbl = UILabel(frame: CGRect(x: pad , y: y, width: Int(w), height: 200))
        self.lbl?.text = "Ola mundo"
        self.lbl?.textColor = UIColor.purple
        self.lbl?.backgroundColor = UIColor(red: 1, green: 1, blue: 0, alpha: 1)
        self.lbl?.textAlignment = NSTextAlignment.center
        
        //MARK: btns
        self.btn = UIButton(frame: CGRect(x: 0, y: btny, width: w, height:100))
        self.btn?.backgroundColor = UIColor.gray
        self.btn?.titleLabel?.text = "btn in code"
        self.btn?.addTarget(self, action: #selector(teste), for: .touchUpInside)
        self.btn?.addTarget(self, action: #selector(setcolor), for: .touchDown)
        self.btn?.addTarget(self, action: #selector(resetcolor), for: .touchUpInside)
 
        for a in arr {
            self.view.addSubview(a)
        }
    }
    

    //MARK: Actions
    //TODO: ola mundo
    @objc
    func teste() {
       self.lbl?.text = "Ola mundo \(c)"
        c += 1
    }

    @objc
    func setcolor() {
            self.btn?.backgroundColor = UIColor.black
    }
    
    @objc
    func resetcolor() {
        self.btn?.backgroundColor = UIColor.gray
    }
    
}
