//
//  ViewController.swift
//  LabB_app3
//
//  Created by Gonçalo Feliciano on 14/05/2019.
//  Copyright © 2019 Gonçalo Feliciano. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txt: UITextField!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var txt2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.txt2.delegate = self
    }

    @IBAction func btn(_ sender: Any) {
        self.lbl.text = self.txt.text
       // self.txt.resignFirstResponder()
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         //self.txt.resignFirstResponder()
       self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.lbl.text = textField.text
        textField.resignFirstResponder()
        return true
    }
}

