//
//  ViewController.swift
//  multiview_labB
//
//  Created by Gonçalo Feliciano on 04/06/2019.
//  Copyright © 2019 Gonçalo Feliciano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtf: UITextField!
 
    @IBAction func btn(_ sender: Any) {
   
        performSegue(withIdentifier: "vc2", sender: self.txtf.text)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let dest = segue.destination as? view2ViewController  else {
 
            let alert = UIAlertController(title: "Erro", message: "Destino Errado", preferredStyle: .alert)
            
            let btn = UIAlertAction(title: "ok", style: .cancel, handler: { a in
                self.teste(a:"")
            })
            
            alert.addAction(btn)
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        
        dest.aux = sender as? String ?? "empty"
        
    }
    
    
    func teste(a:String)  {
        print("Ola Mundo \(a)")
    }
}



