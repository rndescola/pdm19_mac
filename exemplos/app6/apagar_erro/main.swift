import Foundation


extension Array{
    
    func media() -> Int {
        
        var aux = 0
        
        for a in self {
            aux += a as! Int
        }
        
        return aux / self.count
    }
}




enum aulas:Error{
    case reporvado
    case oral(med:Int)
    case notaInvalida(nota1:Int, nota2:Int)
}

/**
Calcula a media de um  **aluno**
 
 - Parameter nota1: Nota 1
 - Parameter nota2: Nota 2

 
 - Throws: `aulas.notaInvalida(nota1: nota1, nota2: nota2)` se  `nota1`  ou `nota1` foram invaldias
 
 - Returns: media do aluno.
 
 */
func aluno(nota1:Int, nota2:Int) throws -> Int {
    
    guard nota1 > 0  && nota1 < 20 else {
        throw aulas.notaInvalida(nota1: nota1, nota2: nota2)
    }
    
    
    let media = (nota1 + nota2)/2
    
    if media < 10 {
        
        if media >= 8 {
            throw aulas.oral(med: media)
        }
        
        throw aulas.reporvado
       
    }
    
    return media
}

func medGlobal(alunos:[Int]) throws -> Int{
    
    let aux = try aluno(nota1: 3, nota2: 1)
    
    print(aux)
    return alunos.media()
    
}


do{
    
    _ = try aluno(nota1: 21, nota2: 8)
  
}catch aulas.reporvado {
    print("Para o ano ha mais")
    
}catch aulas.oral(let nota) {
    print("O aluno vai a oral com \(nota)")
}catch{
    
    print("outro Erro: \(error) ")
    
}catch aulas.notaInvalida(let a, let b) {
    
    print("Nota Invalida notas Lançadas: \(a) e \(b)")
    
}




